<?php

namespace VoodooSMS\LaravelMetrics\Controllers;

use Illuminate\Http\JsonResponse;
use VoodooSMS\LaravelMetrics\Interfaces\PrometheusExporter;

class PrometheusController
{
    public function metrics(PrometheusExporter $prometheus)
    {
        return $prometheus->render(
            $prometheus->export()
        );
    }
}
