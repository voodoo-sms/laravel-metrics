<?php

namespace VoodooSMS\LaravelMetrics\Interfaces;

use Illuminate\Support\Collection;

interface Collector
{
    /**
     * Collect the metrics.
     *
     * @return Collection
     */
    public function collect(): Collection;

    /**
     * Load the metrics ready for collection.
     *
     * @return self
     */
    public function loadMetrics();

    /**
     * Autodiscover the metrics.
     *
     * @return Collection
     */
    public function discoverMetrics(): Collection;

    /**
     * Whether or not metric auto-discovery is enabled.
     *
     * @return bool
     */
    public function shouldDiscoverMetrics(): bool;

    /**
     * Get a collection of hard-coded metrics to collect
     * from the config.
     *
     * @return Collection
     */
    public function getstaticMetrics(): Collection;
}
