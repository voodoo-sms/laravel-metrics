<?php

namespace VoodooSMS\LaravelMetrics\Interfaces;

use Illuminate\Http\Response;
use Illuminate\Support\Collection;

interface PrometheusExporter
{
    /**
     * Get the prometheus metrics.
     *
     * @return Collection
     */
    public function export(): Collection;

    /**
     * Render the prometheus metrics.
     *
     * @param Collection $metrics
     * @return Response
     */
    public function render(Collection $metrics);
}
