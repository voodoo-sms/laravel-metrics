<?php

namespace VoodooSMS\LaravelMetrics\Interfaces;

interface CachedMetric
{
    /**
     * Return the cache key to store the metrics.
     *
     * @return string
     */
    public function getCacheKey(): string;

    /**
     * Return the cached metric value.
     *
     * @return mixed
     */
    public function getCachedValue();

    /**
     * Store the value in the cache.
     *
     * @param mixed $value
     * @return array
     */
    public function storeCachedValue($value): array;
}
