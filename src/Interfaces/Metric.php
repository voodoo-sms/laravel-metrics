<?php

namespace VoodooSMS\LaravelMetrics\Interfaces;

interface Metric
{
    /**
     * Get the key/name of the metric.
     *
     * @return string
     */
    public function key(): string;

    /**
     * Get the metric value.
     *
     * @return mixed
     */
    public function value();

    /**
     * Collect the metric value.
     *
     * @return static
     */
    public function collect();

    /**
     * Get the channels the metric should emit on.
     *
     * @return array
     */
    public function getChannels(): array;

    /**
     * Whether the metric is a count metric or not.
     *
     * @return bool
     */
    public function isCountable(): bool;

    /**
     * Get additional context/labels for the metric
     *
     * @return array
     */
    public function getContext(): array;
}
