<?php

namespace VoodooSMS\LaravelMetrics\Interfaces;

interface Channel
{
    /**
     * Send the metric via the channel.
     *
     * @param Metric $metric
     * @return void
     */
    public function handle(Metric $metric): void;
}
