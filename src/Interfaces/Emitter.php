<?php

namespace VoodooSMS\LaravelMetrics\Interfaces;

use Illuminate\Support\Collection;

interface Emitter
{
    /**
     * Emit the metrics down the desired channels.
     *
     * @param Collection $metrics
     * @return void
     */
    public function emit(Collection $metrics): void;
}
