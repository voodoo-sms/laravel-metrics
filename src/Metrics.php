<?php

namespace VoodooSMS\LaravelMetrics;

use Illuminate\Support\Facades\Route;
use VoodooSMS\LaravelMetrics\Controllers\PrometheusController;
use VoodooSMS\LaravelMetrics\Interfaces\Collector;
use VoodooSMS\LaravelMetrics\Interfaces\Emitter;

class Metrics
{
    private Collector $collector;
    private Emitter $emitter;

    public function __construct(Collector $collector, Emitter $emitter)
    {
        $this->collector = $collector;
        $this->emitter = $emitter;
    }

    /**
     * Collect and emit the metrics.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->emitter->emit(
            $this->collector->loadMetrics()->collect()
        );
    }

    /**
     * Register the packages routes.
     *
     * @return void
     */
    public static function routes(): void
    {
        if (request()->getPort() === config('metrics.prometheus.port')) {
            Route::get('/metrics', [PrometheusController::class, 'metrics'])->name('laravel-metrics.prometheus');
        }
    }
}
