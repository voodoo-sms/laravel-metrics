<?php

use VoodooSMS\LaravelMetrics\Channels\StackChannel;

return [
    /**
     * The directory path for metrics autodiscovery.
     */
    'path' => app('path') . '/Metrics',

    /**
     * The namespace for metrics autodiscovery.
     */
    'namespace' => 'App\\Metrics',

    /**
     * This determines whether or not the metrics collector will
     * autodicover classes in the App\Metrics folder.
     */
    'autodiscover' => (bool) env('METRICS_AUTODISCOVER', true),

    /**
     * This is a list of hard-coded metrics to process in the collector.
     * If `metrics.autodiscover` is `true` then this is ignored.
     */
    'metrics' => [
        //
    ],

    'reporting' => [
        /**
         * The channels where the metrics should be reported.
         */
        'channels' => [
            StackChannel::class
        ],

        /**
         * The interval in seconds that metrics are collected and emitted at.
         */
        'interval' => (int) env('METRICS_INTERVAL', 5),
    ],

    /**
     * Prometheus specific config.
     */
    'prometheus' => [
        /**
         * The port that the /metrics endpoint is exposed on.
         * Note: you will need to update your webserver to listen on this port.
         */
        'port' => env('PROMETHEUS_ENDPOINT_PORT', 8080),
    ],

    'cache' => [
        /**
         * The prefix for the cache key. This allows you to use the same
         * metric name across multiple projects with the same cache store
         * using the same metric keys without conflicts.
         */
        'prefix' => 'laravel-metrics:' . env('APP_NAME') . ':',

        /**
         * How long in seconds should the result stay in the cache.
         */
        'ttl' => 60,

        'redis' => [
            /**
             * The name of the redis connection to use.
             */
            'connection' => 'default',

            'database' => env('METRICS_REDIS_DATABASE', 9),

            'prefix' => env('METRCIS_REDIS_PREFIX', ''),
        ],
    ],
];
