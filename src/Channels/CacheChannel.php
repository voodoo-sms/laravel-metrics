<?php

namespace VoodooSMS\LaravelMetrics\Channels;

use InvalidArgumentException;
use VoodooSMS\LaravelMetrics\Interfaces\CachedMetric;
use VoodooSMS\LaravelMetrics\Interfaces\Channel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;

class CacheChannel implements Channel
{
    /**
     * Send the metric via the channel.
     *
     * @param CachedMetric $metric
     * @return void
     */
    public function handle(Metric $metric): void
    {
        if (!$metric instanceof CachedMetric) {
            throw new InvalidArgumentException(
                get_class($metric) . ' is not an instace of ' . CachedMetric::class
            );
        }

        $metric->storeCachedValue(
            $metric->value()
        );
    }
}
