<?php

namespace VoodooSMS\LaravelMetrics\Channels;

use VoodooSMS\LaravelMetrics\Abstracts\LogChannel;

class StackChannel extends LogChannel
{
    /**
     * {@inheritDoc}
     */
    public function getChannel(): string
    {
        return 'stack';
    }
}
