<?php

namespace VoodooSMS\LaravelMetrics\Channels;

use Illuminate\Support\Collection;
use InvalidArgumentException;
use VoodooSMS\LaravelMetrics\Interfaces\Channel;
use VoodooSMS\LaravelMetrics\Interfaces\Collector;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use VoodooSMS\LaravelMetrics\Interfaces\PrometheusExporter;
use VoodooSMS\LaravelMetrics\Interfaces\PrometheusMetric;
use VoodooSMS\LaravelMetrics\Utils\PrometheusRenderer;

class PrometheusChannel implements Channel, PrometheusExporter
{
    /**
     * Send the metric via the channel.
     *
     * @param PrometheusMetric $metric
     * @return void
     */
    public function handle(Metric $metric): void
    {
        if (!$metric instanceof PrometheusMetric) {
            throw new InvalidArgumentException(
                get_class($metric) . ' is not an instace of ' . PrometheusMetric::class
            );
        }

        $metric->storeCachedValue(
            $metric->value()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function export(): Collection
    {
        $metrics = collect();

        foreach (app()->make(Collector::class)->loadMetrics()->metrics as $metric) {
            if ($metric instanceof PrometheusMetric) {
                $metrics->push($metric);
            }
        }

        return $metrics;
    }

    /**
     * {@inheritDoc}
     */
    public function render(Collection $metrics)
    {
        $response = '';

        /** @var PrometheusMetric */
        foreach ($metrics as $metric) {
            $response .= PrometheusRenderer::render($metric) . PHP_EOL;
        }

        return response($response)->header('Content-Type', 'text/plain');
    }
}
