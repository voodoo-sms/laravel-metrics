<?php

namespace VoodooSMS\LaravelMetrics\Channels;

use VoodooSMS\LaravelMetrics\Abstracts\LogChannel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;

class DatadogChannel extends LogChannel
{
    /**
     * {@inheritDoc}
     */
    public function getChannel(): string
    {
        return 'datadog';
    }

    /**
     * {@inheritDoc}
     */
    public function getContext(Metric $metric): array
    {
        $context = parent::getContext($metric);
        $context['ddqueue'] = false;

        return $context;
    }
}
