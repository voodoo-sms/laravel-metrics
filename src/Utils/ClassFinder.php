<?php

namespace VoodooSMS\LaravelMetrics\Utils;

use Illuminate\Support\Facades\File;

class ClassFinder
{
    /**
     * Get a list of valid classes from a directory (recursive).
     *
     * @param string $path
     * @param string $namespace
     * @return string[]
     */
    public static function find(string $path, string $namespace): array
    {
        $files = File::allFiles($path);
        $classes = [];

        foreach ($files as $file) {
            $subDirs = str_replace(
                '/',
                '\\',
                str_replace($path, '', $file->getPath())
            );

            $className = $namespace . $subDirs .
                '\\' . explode('.', $file->getFilename())[0];

            if (class_exists($className)) {
                $classes[] = $className;
            }
        }

        return $classes;
    }
}
