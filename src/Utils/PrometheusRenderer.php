<?php

namespace VoodooSMS\LaravelMetrics\Utils;

use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use VoodooSMS\LaravelMetrics\Interfaces\PrometheusMetric;

class PrometheusRenderer
{
    /**
     * Render a metric in the prometheus format.
     *
     * @param PrometheusMetric|Metric $metric
     * @return string
     */
    public static function render($metric): string
    {
        return $metric->getPrometheusKey() . ' ' . $metric->getCachedValue();
    }
}
