<?php

namespace VoodooSMS\LaravelMetrics;

use Illuminate\Support\ServiceProvider;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;
use VoodooSMS\LaravelMetrics\Channels\PrometheusChannel;
use VoodooSMS\LaravelMetrics\Collector;
use VoodooSMS\LaravelMetrics\Commands\CollectCommand;
use VoodooSMS\LaravelMetrics\Commands\EmitCommand;
use VoodooSMS\LaravelMetrics\Commands\Generators\MakeChannelCommand;
use VoodooSMS\LaravelMetrics\Commands\Generators\MakeMetricCommand;
use VoodooSMS\LaravelMetrics\Emitter;
use VoodooSMS\LaravelMetrics\Interfaces\Collector as CollectorInterface;
use VoodooSMS\LaravelMetrics\Interfaces\Emitter as EmitterInterface;
use VoodooSMS\LaravelMetrics\Interfaces\PrometheusExporter;

class LaravelMetricsServiceProvider extends ServiceProvider
{

    private static array $commands = [
        CollectCommand::class,
        EmitCommand::class,
        MakeMetricCommand::class,
        MakeChannelCommand::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/Config/metrics.php', 'metrics');

        $this->app->instance(
            'metricPath',
            config('metrics.path')
        );

        $this->app->bind(EmitterInterface::class, function () {
            return new Emitter();
        });

        $this->app->bind(CollectorInterface::class, function ($app) {
            return new Collector(
                config('metrics.path'),
                config('metrics.namespace')
            );
        });

        $this->app->bind(Metrics::class, function ($app) {
            return new Metrics(
                $app->make(CollectorInterface::class),
                $app->make(EmitterInterface::class),
            );
        });

        $this->app->bind(PrometheusExporter::class, PrometheusChannel::class);

        $this->storeRedisConnection(
            CachedMetric::REDIS_CONNECTION,
            $this->buildRedisConfig()
        );
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands($this::$commands);

            $this->publishes([
                __DIR__ . '/Config/metrics.php' => config_path('metrics.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/Stubs/' => resource_path('stubs/Metrics'),
            ], 'stubs');
        }
    }

    /**
     * Build a new redis connection array with no prefix.
     *
     * @return array
     */
    private function buildRedisConfig(): array
    {
        $config = config('database.redis.' . config('metrics.cache.redis.connection'));

        $options = config('database.redis.options');
        $options['prefix'] = config('metrics.cache.redis.prefix');

        $config['options'] = $options;
        $config['database'] = config('metrics.cache.redis.database');

        return $config;
    }

    /**
     * Store the new redis connection in the config.
     *
     * @param string $name
     * @param array $connection
     * @return void
     */
    private function storeRedisConnection(string $name, array $connection): void
    {
        config([
            'database.redis.' . $name => $connection
        ]);
    }
}
