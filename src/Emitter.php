<?php

namespace VoodooSMS\LaravelMetrics;

use Illuminate\Support\Collection;
use VoodooSMS\LaravelMetrics\Interfaces\Channel;
use VoodooSMS\LaravelMetrics\Interfaces\Emitter as EmitterInterface;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;

class Emitter implements EmitterInterface
{
    /**
     * {@inheritDoc}
     */
    public function emit(Collection $metrics): void
    {
        /** @var Metric */
        foreach ($metrics as $metric) {
            /** @var Channel */
            foreach ($metric->getChannels() as $channel) {
                $channel->handle($metric);
            }
        }
    }
}
