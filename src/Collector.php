<?php

namespace VoodooSMS\LaravelMetrics;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;
use Symfony\Component\Finder\Finder;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use VoodooSMS\LaravelMetrics\Interfaces\Collector as CollectorInterface;
use VoodooSMS\LaravelMetrics\Utils\ClassFinder;

class Collector implements CollectorInterface
{
    private string $metricPath;
    private string $namespace;

    public Collection $metrics;

    public function __construct(string $metricPath, string $namespace)
    {
        $this->metricPath = $metricPath;
        $this->namespace = $namespace;
    }

    /**
     * {@inheritDoc}
     */
    public function collect(): Collection
    {
        $metrics = collect();

        /** @var Metric */
        foreach ($this->metrics as $metric) {
            // This collects the metric value into memory/cache
            $metric->value();

            $metrics->push($metric);
        }

        return $metrics;
    }

    /**
     * {@inheritDoc}
     */
    public function loadMetrics()
    {
        $this->metrics = $this->getstaticMetrics();

        if ($this->shouldDiscoverMetrics()) {
            $this->metrics = $this->discoverMetrics();
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function discoverMetrics(): Collection
    {
        $metrics = collect();

        try {
            foreach (ClassFinder::find($this->metricPath, $this->namespace) as $file) {
                if (($class = $this->isValidMetric($file)) !== false) {
                    $metrics->push($class);
                }
            }
        } catch (DirectoryNotFoundException $e) {
            //
        }

        return $metrics;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldDiscoverMetrics(): bool
    {
        return config('metrics.autodiscover');
    }

    /**
     * {@inheritDoc}
     */
    public function getstaticMetrics(): Collection
    {
        $metrics = collect();

        foreach (config('metrics.metrics') as $class) {
            if (class_exists($class)) {
                if (($class = $this->isValidMetric($class)) !== false) {
                    $metrics->push($class);
                }
            }
        };

        return $metrics;
    }

    /**
     * Determine if a class is a valid metric.
     *
     * @param string $class
     * @return bool|Metric
     */
    private function isValidMetric(string $class)
    {
        try {
            $class = app()->make($class);
        } catch (BindingResolutionException $e) {
            return false;
        }

        return $class instanceof Metric
            ? $class
            : false;
    }
}
