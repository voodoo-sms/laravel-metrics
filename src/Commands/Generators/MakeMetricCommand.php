<?php

namespace VoodooSMS\LaravelMetrics\Commands\Generators;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeMetricCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:metric';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new metric';

    protected $type = 'Metric';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ((bool) $this->option('prometheus') === true) {
            return file_exists(resource_path('stubs/Metrics/PrometheusMetricStub.stub'))
                ? resource_path('stubs/Metrics/PrometheusMetricStub.stub')
                : __DIR__ . '/../../Stubs/PrometheusMetricStub.stub';
        }

        if ((bool) $this->option('cached') === true) {
            return file_exists(resource_path('stubs/Metrics/CachedMetricStub.stub'))
                ? resource_path('stubs/Metrics/CachedMetricStub.stub')
                : __DIR__ . '/../../Stubs/CachedMetricStub.stub';
        }

        return file_exists(resource_path('stubs/Metrics/MetricStub.stub'))
            ? resource_path('stubs/Metrics/MetricStub.stub')
            : __DIR__ . '/../../Stubs/MetricStub.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return config('metrics.namespace');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the action.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['prometheus', null, InputOption::VALUE_NONE, 'Whether to make a prometheus metric or a standard metric.'],
            ['cached', null, InputOption::VALUE_NONE, 'Whether to make a cached metric or a standard metric.'],
        ];
    }
}
