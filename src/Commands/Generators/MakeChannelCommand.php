<?php

namespace VoodooSMS\LaravelMetrics\Commands\Generators;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeChannelCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:metrics-channel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new metrics channel';

    protected $type = 'Channel';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return file_exists(resource_path('stubs/Metrics/ChannelStub.stub'))
            ? resource_path('stubs/Metrics/ChannelStub.stub')
            : __DIR__ . '/../../Stubs/ChannelStub.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return config('metrics.namespace') . '/Channels';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the action.'],
        ];
    }
}
