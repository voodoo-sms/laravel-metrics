<?php

namespace VoodooSMS\LaravelMetrics\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use VoodooSMS\LaravelMetrics\Metrics;

class EmitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'metrics:emit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect and emit metrics.';

    private bool $run = true;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Metrics $metrics)
    {
        pcntl_async_signals(true);

        pcntl_signal(SIGINT, [$this, 'shutdown']); // Call $this->shutdown() on SIGINT
        pcntl_signal(SIGTERM, [$this, 'shutdown']); // Call $this->shutdown() on SIGTERM

        $interval = (int) config('metrics.reporting.interval');

        $this->info('Emitting metrics at an interval of ' . $interval . ' seconds');

        while ($this->run) {
            $this->info('Emitting metrics');
            $metrics->handle();

            sleep($interval);
        }
    }

    /**
     * Shutdown gracefully
     *
     * @return void
     */
    public function shutdown(): void
    {
        $this->info('Gracefully stopping worker...');

        $this->run = false;
    }

    public function line($string, $style = null, $verbosity = null)
    {
        $string = Carbon::now()->format('Y-m-d H:i:s') . ' - ' . $string;

        $styled = $style ? "<$style>$string</$style>" : $string;

        $this->output->writeln($styled, $this->parseVerbosity($verbosity));
    }
}
