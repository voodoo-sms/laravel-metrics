<?php

namespace VoodooSMS\LaravelMetrics\Commands;

use Illuminate\Console\Command;
use VoodooSMS\LaravelMetrics\Interfaces\Collector;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;

class CollectCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'metrics:collect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect and display metrics.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Collector $collector)
    {
        $metrics = [];

        /** @var Metric */
        foreach ($collector->loadMetrics()->collect() as $metric) {
            $metrics[] = [
                'key' => $metric->key(),
                'value' => $metric->value(),
            ];
        }

        $this->table(
            ['Metric', 'Value'],
            $metrics
        );
    }
}
