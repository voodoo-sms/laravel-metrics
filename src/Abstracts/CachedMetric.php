<?php

namespace VoodooSMS\LaravelMetrics\Abstracts;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use VoodooSMS\LaravelMetrics\Abstracts\Metric as MetricAbstract;
use VoodooSMS\LaravelMetrics\Interfaces\CachedMetric as CachedMetricInterface;

abstract class CachedMetric extends MetricAbstract implements CachedMetricInterface
{
    public const REDIS_CONNECTION = 'metrics';

    /**
     * {@inheritDoc}
     */
    public function getCacheKey(): string
    {
        return config('metrics.cache.prefix') . $this->key();
    }

    /**
     * {@inheritDoc}
     */
    public function getCachedValue()
    {
        $value = json_decode(
            Redis::connection(self::REDIS_CONNECTION)->get($this->getCacheKey()),
            true
        );

        if ($value === null) {
            $value = $this->storeCachedValue($this->value());
        }

        $return = $value['value'];
        settype($return, $value['type']);

        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function storeCachedValue($value): array
    {
        $value = $this->buildArrayToStore($value);

        Redis::connection(self::REDIS_CONNECTION)->set(
            $this->getCacheKey(),
            json_encode($value, JSON_UNESCAPED_SLASHES),
            'EX',
            config('metrics.cache.ttl')
        );

        return $value;
    }

    protected function buildArrayToStore($value): array
    {
        return [
            'value' => $this->value(),
            'type' => gettype($this->value),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function value()
    {
        if ($this->value === null) {
            $this->collect();

            $this->storeCachedValue(
                $this->value
            );
        }

        return $this->value;
    }
}
