<?php

namespace VoodooSMS\LaravelMetrics\Abstracts;

use Illuminate\Support\Facades\Cache;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;
use VoodooSMS\LaravelMetrics\Interfaces\Metric as MetricInterface;
use VoodooSMS\LaravelMetrics\Interfaces\PrometheusMetric as PrometheusMetricInterface;

abstract class PrometheusMetric extends CachedMetric implements MetricInterface, PrometheusMetricInterface
{
    protected function buildArrayToStore($value): array
    {
        return [
            'value' => $this->value(),
            'type' => gettype($this->value),
            'prometheus_key' => $this->getPrometheusKey(),
        ];
    }

    public function getCacheKey(): string
    {
        return config('metrics.cache.prefix') . 'prometheus:' . $this->key();
    }
}
