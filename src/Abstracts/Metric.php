<?php

namespace VoodooSMS\LaravelMetrics\Abstracts;

use VoodooSMS\LaravelMetrics\Channels\StackChannel;
use VoodooSMS\LaravelMetrics\Interfaces\Channel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric as MetricInterface;

abstract class Metric implements MetricInterface
{
    protected bool $countable = false;

    protected ?array $channels = null;

    protected $value = null;

    protected $context = [];

    /**
     * {@inheritDoc}
     */
    public function getChannels(): array
    {
        $channels = $this->channels ?? config('metrics.reporting.channels');

        for ($i = 0; $i < count($channels); $i++) {
            $channel = $channels[$i];

            if (class_exists($channel)) {
                $channel = app()->make($channel);

                if ($channel instanceof Channel) {
                    $channels[$i] = $channel;

                    continue;
                }
            }

            unset($channels[$i]);
        }

        return array_values($channels);
    }

    /**
     * {@inheritDoc}
     */
    public function value()
    {
        if ($this->value === null) {
            $this->collect();
        }

        return $this->value;
    }

    /**
     * {@inheritDoc}
     */
    public function isCountable(): bool
    {
        return $this->countable;
    }

    /**
     * {@inheritDoc}
     */
    public function getContext(): array
    {
        return $this->context;
    }
}
