<?php

namespace VoodooSMS\LaravelMetrics\Abstracts;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use VoodooSMS\LaravelMetrics\Interfaces\Channel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;

abstract class LogChannel implements Channel
{
    /**
     * Get the log channel to write to.
     *
     * @return string
     */
    abstract public function getChannel(): string;

    /**
     * {@inheritDoc}
     */
    public function handle(Metric $metric): void
    {
        Log::channel($this->getChannel())->info(
            $metric->key(),
            $this->getContext($metric)
        );
    }

    /**
     * Get the log context.
     *
     * @param string $key
     * @param mixed $value
     * @return array
     */
    public function getContext(Metric $metric): array
    {
        $key = $metric->isCountable() ? 'count' : 'value';

        $ctx = [
            'event' => Str::upper($metric->key()),
            $key => $metric->value(),
        ];
        return array_merge($ctx, $metric->getContext());
    }
}
