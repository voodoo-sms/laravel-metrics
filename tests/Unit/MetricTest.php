<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit;

use VoodooSMS\LaravelMetrics\Channels\StackChannel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use VoodooSMS\LaravelMetrics\Metrics;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;

class MetricTest extends TestCase
{
    private Metric $metric;

    public function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('metrics.reporting.channels', [
            StackChannel::class,
        ]);

        $this->metric = new TestMetric;
    }

    public function test_get_channels_only_returns_instances_of_channel_interface()
    {
        $this->app['config']->set('metrics.reporting.channels', [
            StackChannel::class,
            Metrics::class,
        ]);

        $this->assertCount(
            1,
            $this->metric->getChannels()
        );
    }

    public function test_get_channels_returns_instantiated_channel_objects()
    {
        $channel = $this->metric->getChannels()[0];

        $this->assertInstanceOf(
            StackChannel::class,
            $channel
        );
    }

    public function test_it_returns_additional_context()
    {
        $this->assertEquals(['bongo' => 'bingo'], $this->metric->getContext());
    }
}
