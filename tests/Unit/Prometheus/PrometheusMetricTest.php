<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Prometheus;

use Illuminate\Support\Facades\Redis;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;
use VoodooSMS\LaravelMetrics\Abstracts\PrometheusMetric;
use VoodooSMS\LaravelMetrics\Channels\PrometheusChannel;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestPrometheusMetric;

class PrometheusMetricTest extends TestCase
{
    private PrometheusMetric $metric;

    public function setUp(): void
    {
        parent::setUp();

        $this->metric = new TestPrometheusMetric;
    }

    public function test_it_stores_a_value_in_the_Redis_when_there_isnt_one_present()
    {
        $this->assertNull(Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey()));

        $this->metric->getCachedValue();

        $this->assertNotNull(Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey()));
    }

    public function test_it_stores_a_value_in_the_Redis_when_there_isnt_one_present_for_value_method()
    {
        $this->assertNull(Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey()));

        $this->metric->value();

        $this->assertNotNull(Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey()));
    }
}
