<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Prometheus;

use Illuminate\Http\Response;
use InvalidArgumentException;
use VoodooSMS\LaravelMetrics\Abstracts\PrometheusMetric;
use VoodooSMS\LaravelMetrics\Channels\PrometheusChannel;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestPrometheusMetric;

class PrometheusExporterTest extends TestCase
{
    private PrometheusMetric $metric;
    private PrometheusChannel $channel;

    public function setUp(): void
    {
        parent::setUp();

        $this->metric = new TestPrometheusMetric;
        $this->channel = new PrometheusChannel;
    }

    public function test_export_only_returns_prometheus_metrics()
    {
        $this->app['config']->set('metrics.metrics', [
            TestMetric::class,
            TestPrometheusMetric::class,
        ]);
        $this->app['config']->set('metrics.autodiscover', false);

        $this->assertCount(
            1,
            $this->channel->export()
        );
    }

    public function test_render_returns_an_instace_of_response()
    {
        $this->app['config']->set('metrics.metrics', [
            TestPrometheusMetric::class,
        ]);
        $this->app['config']->set('metrics.autodiscover', false);

        $this->assertInstanceOf(
            Response::class,
            $this->channel->render(
                $this->channel->export()
            )
        );
    }
}
