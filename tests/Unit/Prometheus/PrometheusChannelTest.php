<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Prometheus;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use InvalidArgumentException;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;
use VoodooSMS\LaravelMetrics\Abstracts\PrometheusMetric;
use VoodooSMS\LaravelMetrics\Channels\PrometheusChannel;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestPrometheusMetric;

class PrometheusChannelTest extends TestCase
{
    private PrometheusMetric $metric;
    private PrometheusChannel $channel;

    public function setUp(): void
    {
        parent::setUp();

        $this->metric = new TestPrometheusMetric;
        $this->channel = new PrometheusChannel;
    }

    public function test_it_stores_a_value_in_the_cache()
    {
        $this->assertNull(Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey()));

        $this->channel->handle($this->metric);

        $this->assertNotNull(Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey()));
    }

    public function test_it_throws_an_exception_when_not_passed_an_instance_of_prometheus_metric()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->channel->handle(new TestMetric);
    }
}
