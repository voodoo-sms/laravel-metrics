<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit;

use Illuminate\Support\Facades\Log;
use TiMacDonald\Log\LogFake;
use VoodooSMS\LaravelMetrics\Channels\StackChannel;
use VoodooSMS\LaravelMetrics\Metrics;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;

class MetricsTest extends TestCase
{
    private Metrics $metrics;

    public function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('metrics.reporting.channels', [
            StackChannel::class,
        ]);
        $this->app['config']->set('metrics.metrics', [
            TestMetric::class,
        ]);
        $this->app['config']->set('metrics.autodiscover', false);

        $this->metrics = app()->make(Metrics::class);
    }

    public function test_it_pushes_a_metric_to_the_reporting_channel()
    {
        Log::swap(new LogFake);

        $this->metrics->handle();

        Log::channel('stack')->assertLogged('info');
    }
}
