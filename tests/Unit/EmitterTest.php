<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit;

use Illuminate\Support\Facades\Log;
use TiMacDonald\Log\LogFake;
use VoodooSMS\LaravelMetrics\Channels\StackChannel;
use VoodooSMS\LaravelMetrics\Emitter;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use VoodooSMS\LaravelMetrics\Metrics;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;

class EmitterTest extends TestCase
{
    private Emitter $emitter;
    private Metric $metric;

    public function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('metrics.reporting.channels', [
            StackChannel::class,
        ]);

        $this->metric = new TestMetric;
        $this->emitter = new Emitter;
    }

    public function test_it_pushes_a_metric_to_the_reporting_channel()
    {
        Log::swap(new LogFake);

        $this->emitter->emit(collect([$this->metric]));

        Log::channel('stack')->assertLogged('info');
    }
}
