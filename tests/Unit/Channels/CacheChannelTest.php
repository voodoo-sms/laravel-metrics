<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Channels;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;
use VoodooSMS\LaravelMetrics\Channels\CacheChannel;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestCacheMetric;

class CacheChannelTest extends TestCase
{
    private CacheChannel $channel;
    private CachedMetric $metric;

    public function setUp(): void
    {
        parent::setUp();

        $this->channel = new CacheChannel;
        $this->metric = new TestCacheMetric;
    }

    public function test_it_stores_a_value_in_the_cache_when_its_empty()
    {
        $this->assertNull(
            Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey())
        );

        $this->channel->handle($this->metric);

        $this->assertNotNull(
            Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey())
        );
    }

    public function test_it_doesnt_refresh_the_cached_value()
    {
        $this->channel->handle($this->metric);

        $original = $this->metric->getCachedValue();
        $this->channel->handle($this->metric);
        $final = $this->metric->getCachedValue();

        $this->assertEquals(
            $original,
            $final
        );
    }
}
