<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Channels;

use Illuminate\Support\Facades\Log;
use TiMacDonald\Log\LogFake;
use VoodooSMS\LaravelMetrics\Channels\StackChannel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;

class StackChannelTest extends TestCase
{
    private Metric $metric;
    private StackChannel $channel;

    public function setUp(): void
    {
        $this->metric = new TestMetric;
        $this->metric->collect();

        $this->channel = new StackChannel;

        Log::swap(new LogFake);
    }

    public function test_it_inputs_a_value_into_the_log()
    {
        $this->channel->handle(
            $this->metric
        );

        Log::channel('stack')->assertLogged('info', function ($message, $context) {
            $this->assertEquals($message, 'phpunit');
            $this->assertEquals($context['bongo'], 'bingo');
            return true;
        });
    }
}
