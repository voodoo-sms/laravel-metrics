<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Channels;

use Illuminate\Support\Facades\Log;
use TiMacDonald\Log\LogFake;
use VoodooSMS\LaravelMetrics\Channels\DatadogChannel;
use VoodooSMS\LaravelMetrics\Interfaces\Metric;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestMetric;

class DatadogChannelTest extends TestCase
{
    private Metric $metric;
    private DatadogChannel $channel;

    public function setUp(): void
    {
        $this->metric = new TestMetric;
        $this->metric->collect();

        $this->channel = new DatadogChannel;

        Log::swap(new LogFake);
    }

    public function test_it_inputs_a_value_into_the_log()
    {
        $this->channel->handle(
            $this->metric
        );

        Log::channel('datadog')->assertLogged('info', function ($message, $context) {
            $this->assertEquals('phpunit', $message);
            $this->assertEquals($context['bongo'], 'bingo');
            return true;
        });
    }
}
