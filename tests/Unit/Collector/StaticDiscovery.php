<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Collector;

use VoodooSMS\LaravelMetrics\Collector;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\OneNotRecursive\NotAMetric;
use VoodooSMS\LaravelMetrics\Tests\Utils\OneNotRecursive\TestMetric;
use VoodooSMS\LaravelMetrics\Tests\Utils\ValidMetricWithParams;

class StaticTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('metrics.autodiscover', false);
    }

    public function test_it_only_returns_valid_metrics()
    {
        $this->app['config']->set(
            'metrics.metrics',
            [
                NotAMetric::class,
                TestMetric::class,
            ]
        );

        $collector = new Collector(__DIR__ . '/../../Utils/OneNotRecursive', 'VoodooSMS\\LaravelMetrics\\Tests\\Utils\\OneNotRecursive');

        $collector->loadMetrics();

        $this->assertCount(
            1,
            $collector->metrics
        );
    }

    public function test_it_skips_a_valid_metric_if_the_container_cannot_instantiate_it()
    {
        $this->app['config']->set(
            'metrics.metrics',
            [
                ValidMetricWithParams::class,
            ]
        );

        $collector = new Collector(__DIR__ . '/../../Utils/OneNotRecursive', 'VoodooSMS\\LaravelMetrics\\Tests\\Utils\\OneNotRecursive');

        $collector->loadMetrics();

        $this->assertCount(
            0,
            $collector->metrics
        );
    }
}
