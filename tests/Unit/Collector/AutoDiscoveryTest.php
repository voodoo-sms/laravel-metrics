<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit\Collector;

use VoodooSMS\LaravelMetrics\Collector;
use VoodooSMS\LaravelMetrics\Tests\TestCase;

class AutoDiscoveryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('metrics.autodiscover', true);
    }

    public function test_it_only_finds_one_metric_class()
    {
        $collector = new Collector(__DIR__ . '/../../Utils/OneNotRecursive', 'VoodooSMS\\LaravelMetrics\\Tests\\Utils\\OneNotRecursive');

        $collector->loadMetrics();

        $this->assertCount(
            1,
            $collector->metrics
        );
    }

    public function test_it_finds_two_metric_classes_recursively()
    {
        $collector = new Collector(__DIR__ . '/../../Utils/TwoRecursive', 'VoodooSMS\\LaravelMetrics\\Tests\\Utils\\TwoRecursive');

        $collector->loadMetrics();

        $this->assertCount(
            2,
            $collector->metrics
        );
    }

    public function test_it_returns_an_empty_collection_for_a_missing_directory()
    {
        $collector = new Collector(__DIR__ . '/../../Utils/Missing', 'VoodooSMS\\LaravelMetrics\\Tests\\Utils\\Missing');

        $collector->loadMetrics();

        $this->assertCount(
            0,
            $collector->metrics
        );
    }
}
