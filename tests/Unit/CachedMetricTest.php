<?php

namespace VoodooSMS\LaravelMetrics\Tests\Unit;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;
use VoodooSMS\LaravelMetrics\Tests\TestCase;
use VoodooSMS\LaravelMetrics\Tests\Utils\TestCacheMetric;

class CachedMetricTest extends TestCase
{
    private TestCacheMetric $metric;

    public function setUp(): void
    {
        parent::setUp();

        $this->metric = new TestCacheMetric;
    }

    public function test_it_stores_a_value_in_the_cache_when_its_empty()
    {
        $this->assertNull(
            Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey())
        );

        $this->metric->value();

        $this->assertNotNull(
            Redis::connection(CachedMetric::REDIS_CONNECTION)->get($this->metric->getCacheKey())
        );
    }

    public function test_it_refreshes_the_cached_value()
    {
        $original = $this->metric->value();
        $this->metric->collect();
        $final = $this->metric->value();

        $this->assertNotEquals(
            $original,
            $final
        );
    }
}
