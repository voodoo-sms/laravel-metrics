<?php

namespace VoodooSMS\LaravelMetrics\Tests\Utils;

use VoodooSMS\LaravelMetrics\Abstracts\Metric;

class TestMetric extends Metric
{
    protected $context = ['bongo' => 'bingo'];

    public function key(): string
    {
        return 'phpunit';
    }

    public function collect()
    {
        $this->value = 5;

        return $this;
    }
}
