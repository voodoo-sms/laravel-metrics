<?php

namespace VoodooSMS\LaravelMetrics\Tests\Utils;

use VoodooSMS\LaravelMetrics\Abstracts\Metric;

class ValidMetricWithParams extends Metric
{
    public function __construct(bool $hello)
    {
        //
    }

    public function key(): string
    {
        return 'ValidMetricWithParams';
    }

    public function collect()
    {
        $this->value = 5;

        return $this;
    }
}
