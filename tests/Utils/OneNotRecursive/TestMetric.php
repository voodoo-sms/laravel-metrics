<?php

namespace VoodooSMS\LaravelMetrics\Tests\Utils\OneNotRecursive;

use VoodooSMS\LaravelMetrics\Abstracts\Metric;

class TestMetric extends Metric
{
    public function key(): string
    {
        return 'pupnit';
    }

    public function collect()
    {
        $this->value = 5;

        return $this;
    }
}
