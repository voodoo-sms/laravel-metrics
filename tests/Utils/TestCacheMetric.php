<?php

namespace VoodooSMS\LaravelMetrics\Tests\Utils;

use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;

class TestCacheMetric extends CachedMetric
{
    public function key(): string
    {
        return 'test-cache';
    }

    public function collect()
    {
        $this->value = rand(0, 100);

        return $this;
    }
}
