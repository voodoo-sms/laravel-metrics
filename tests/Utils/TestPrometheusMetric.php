<?php

namespace VoodooSMS\LaravelMetrics\Tests\Utils;

use VoodooSMS\LaravelMetrics\Abstracts\PrometheusMetric;

class TestPrometheusMetric extends PrometheusMetric
{
    public function key(): string
    {
        return 'pupnit';
    }

    public function collect()
    {
        $this->value = 5;

        return $this;
    }

    public function getPrometheusKey(): string
    {
        return 'phpunit{test="unit"}';
    }
}
