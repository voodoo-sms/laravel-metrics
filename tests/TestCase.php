<?php

namespace VoodooSMS\LaravelMetrics\Tests;

use Illuminate\Support\Facades\Redis;
use VoodooSMS\LaravelMetrics\Abstracts\CachedMetric;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->artisan('migrate', ['--database' => 'testbench'])->run();
        Redis::connection(CachedMetric::REDIS_CONNECTION)->flushDb();
    }

    protected function getPackageProviders($app)
    {
        return ['VoodooSMS\LaravelMetrics\LaravelMetricsServiceProvider'];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
        $app['config']->set('restapi.connection', 'testbench');
    }
}
