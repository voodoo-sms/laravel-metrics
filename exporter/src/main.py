from fastapi import Depends, FastAPI
from fastapi.responses import PlainTextResponse
from Utils.Settings import Settings
from Utils.Redis import Redis
from Utils.Exporter import Exporter
from functools import lru_cache

app = FastAPI(openapi_url=None)

@lru_cache()
def settings():
    return Settings()

def redis(settings: Settings = Depends(settings)) -> Redis:
    return Redis(
        host=settings.REDIS_HOST,
        port=settings.REDIS_PORT,
        password=settings.REDIS_PASSWORD,
        database=settings.REDIS_DATABASE,
        prefix=settings.REDIS_PREFIX
    )

def exporter(client: Redis = Depends(redis)) -> Exporter:
    return Exporter(client)

@app.get('/metrics', response_class=PlainTextResponse)
async def metrics(exporter: Exporter = Depends(exporter)):
    return exporter.export()
