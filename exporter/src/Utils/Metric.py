class Metric:
    key: str
    type: str

    def __init__(self, key: str, value, type: str) -> None:
        self.key = key
        self.value = value
        self.type = type

    def __getKey(self) -> str:
        return str(self.key)

    def __getValue(self):
        return str(self.value)

    def __str__(self) -> str:
        return self.__getKey() + ' ' + self.__getValue()
