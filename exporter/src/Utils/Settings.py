from pydantic import BaseSettings
from dotenv import load_dotenv

class Settings(BaseSettings):
    REDIS_HOST: str
    REDIS_PORT: int
    REDIS_PASSWORD: str = None
    REDIS_DATABASE: int = 9
    REDIS_PREFIX: str = ''

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'

