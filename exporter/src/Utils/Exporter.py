import json
from Utils.Redis import Redis
from Utils.Metric import Metric

class Exporter:
    def __init__(self, client: Redis) -> None:
        self.client = client

    def export(self) -> str:
        metrics = []
        for key in self.client.keys():
            metric = json.loads(self.client.get(key))
            metric = Metric(
                key=metric['prometheus_key'],
                value=metric['value'],
                type=metric['type']
            )
            metrics.append(metric)

        output = ''

        for metric in metrics:
            output += str(metric) + '\n'

        return output
