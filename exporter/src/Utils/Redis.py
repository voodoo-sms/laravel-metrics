from redis import Redis as redisc

class Redis:
    host: str
    port: int
    password: str
    database: int
    prefix: str
    client: redisc

    def __init__(self, host: str, port: int, password: str, database: int, prefix: str) -> None:
        self.host = host
        self.port = port
        self.database = database
        self.prefix = prefix
        if password == 'null':
            self.password = None
        else:
            self.password = password
        self.client = self.__buildClient()

    def get(self, key: str) -> list:
        return self.client.get(key)

    def keys(self):
        return self.client.keys(self.prefix + 'prometheus:*')

    def __buildClient(self) -> redisc:
        return redisc(
            host=self.host,
            port=self.port,
            password=self.password,
            db=self.database
        )
